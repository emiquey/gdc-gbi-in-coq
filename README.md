# gdc-gbi-in-coq

Coq formalization for https://pauillac.inria.fr/~herbelin/articles/lics-BreHer21-barchoice.pdf

Formalized definitions and propositions are yellowed in the companion pdf.
